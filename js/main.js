window.addEventListener('load', function () {
    'use strict';
    const navBurguer = document.querySelector('.navbar-burger');
    navBurguer.addEventListener('click', function () {
        const target = this.dataset.target;
        const $target = document.querySelector(`#${target}`);
        navBurguer.classList.toggle('is-active');
        $target.classList.toggle('is-active');

    });
    const submitBtn = document.querySelector('#btnSubmit');
    submitBtn.addEventListener('click', checkForm);
    function checkForm(e){
        const msg = document.querySelector('textarea').value;
        if(msg.length <= 1){
            alert('please enter a message ');
            e.preventDefault();
        }
    }
});